import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class ClipboardDemo extends JPanel implements ClipboardOwner {

    private JTextArea textArea;
    
    public ClipboardDemo() {
        
        this.setLayout(new FlowLayout());

        // Create a text area for copy and paste tests
        // Note that by default, text widgets will support keyboard shortcuts
        // for copy/paste
        textArea = new JTextArea();
        textArea.setMinimumSize(new Dimension(300, 150));
        textArea.setPreferredSize(textArea.getMinimumSize());
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        this.add(textArea);

        textArea.setText("Lorem ipsum dolor sit amet, semper dissentiet concludaturque an has, " +
                         "case vivendo vix an. Probo tempor laoreet quo ad.");

        // Create copy/cut/paste buttons to support manual copying and pasting
        JButton copyButton = new JButton("Copy");
        JButton cutButton = new JButton("Cut");
        JButton pasteButton = new JButton("Paste");
        this.add(copyButton);
        this.add(cutButton);
        this.add(pasteButton);

        // Add action listeners to perform the clipboard operations
        copyButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doCopy();
            }
        });

        cutButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doCut();
            }
        });

        pasteButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doPaste();
            }
        });
    }
    
    private void doCopy() {

        System.out.println(String.format("COPY: `%s`", textArea.getSelectedText()));
        
        // Get the system clipboard
        Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();

        // Create a transferable object encapsulating all the info for the copy
        Transferable transferObject = new Transferable() {

            private String text = textArea.getSelectedText();
            
            // Returns the copy data
            public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException ,IOException {
                System.out.println("  Transferable.getTransferData as " + flavor);
                if (flavor.equals(DataFlavor.stringFlavor)) {
                    return text;
                }
                throw new UnsupportedFlavorException(flavor);
            }
            
            // Returns the set of data formats we can provide
            public DataFlavor[] getTransferDataFlavors() {
                System.out.println("  Transferable.getTransferDataFlavors");
                return new DataFlavor[] { DataFlavor.stringFlavor };
            }
            
            // Indicates whether we can provide data in the specified format
            public boolean isDataFlavorSupported(DataFlavor flavor) {
                System.out.println("  Transferable.isDataFlavorSupported: " + flavor);
                return flavor.equals(DataFlavor.stringFlavor);
            }
        };
        
        // Now set the contents of the clipboard to our transferable object
        // NOTE: The second argument "this" tells the system that this 
        //       object would like to be the owner of the clipboard.
        //       As such, this object must implement the ClipboardOwner interface
        System.out.println("COPY: set system clipboard to Transferable");
        cb.setContents(transferObject, this);
    }

    private void doCut() {

        System.out.println("CUT");

        // cut is just a copy that also removes data from document
        doCopy();
        textArea.replaceSelection("");
    }
    
    private void doPaste() {

        System.out.println("PASTE");
        
        // Grab system clipboard
        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

        System.out.println(String.format("PASTE: %d available flavours ... ",
                systemClipboard.getAvailableDataFlavors().length));
        for (DataFlavor f: systemClipboard.getAvailableDataFlavors()) {
            System.out.println("  " + f.getHumanPresentableName() + "  " + f.toString());
        }

        // Check if we can get the data as a string
        if (systemClipboard.isDataFlavorAvailable(DataFlavor.stringFlavor)) {
            System.out.println("PASTE: DataFlavor.stringFlavor available");
            try {
                // Grab the data, set our text area to the data
                String theText = (String)systemClipboard.getData(DataFlavor.stringFlavor);
                textArea.replaceSelection(theText);
                System.out.println("PASTE: '" + theText + "'");
            } catch (UnsupportedFlavorException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("PASTE: DataFlavor.stringFlavor NOT available");
        }
    }

    // Implement the ClipboardOwner interface
    public void lostOwnership(Clipboard clipboard, Transferable contents) {
        System.out.println("ClipboardOwner: lost clipboard ownership");
    }

    public static void main(String[] args) {
        JFrame f = new JFrame("Clipboard");
        f.getContentPane().add(new ClipboardDemo());
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        f.pack();
        f.setVisible(true);
    }
}
